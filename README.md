# GitLab to EKS

This Repository contains sample nodejs application code and CI/CD pipeline to deploy Nodejs application into EKS Cluster.

This Repository is the code repo for [this blog post](https://blog.heinux-training.net/building-a-gitlab-cicd-pipeline-for-aws-eks-cluster-deployment).
